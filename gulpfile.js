const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');

const cssFiles = [
	// './node_modules/normalize.css/normalize.css',
	'./src/css/style.css',
	'./src/css/other.css'
];

const sassFiles = [
	// './src/sass/_default.sass',
	'./src/sass/style.sass',
	// './src/sass/_option.sass',
];

const jsFiles = [
	'./src/js/lib.js',
	'./src/js/main.js'
];

function htmlBuil(){
	return gulp.src(htmlBuil)
		.pipe(gulp.dest('./build/'))
		.pipe(browserSync.stream());
}

function styles(){
	return gulp.src(cssFiles)
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('all.css'))
		.pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCss({
        	leevel :2
        }))
		.pipe(gulp.dest('./build/css'))
		.pipe(browserSync.stream());
}

function styleSass() {
	return gulp.src(sassFiles)
    	.pipe(sass({
			outputStyle: "expanded"
		}).on('error', sass.logError))
		.pipe(gulp.dest('./src/css/'))
		.pipe(browserSync.stream());
}

//для пущего сжатия JS 
//.pipe(uglify({
//	toplevel: true
//}))

function scripts(){
	return gulp.src(jsFiles)
		.pipe(concat('all.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./build/js'))
		.pipe(browserSync.stream());
}

function watch(){
	browserSync.init({
        server: {
            baseDir: "./"
        }
    });
	gulp.watch('./src/sass/**/*.sass', styleSass);
	gulp.watch('./src/css/**/*.css', styles);
	gulp.watch('./src/js/**/*.js', scripts);
	gulp.watch('./*.html').on('change', browserSync.reload);
}

function clean() {
	return del(['build/*']);
}

gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('watch', watch);
gulp.task('styleSass', styleSass);

gulp.task('build', gulp.series(clean, gulp.parallel(styles, scripts) ) );